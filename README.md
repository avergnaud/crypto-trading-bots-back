# installation
npm install

# run
gulp

npm start

# curl

http://localhost:3001/api/v1/bots

curl -X DELETE http://localhost:3001/api/v1/bots/1

curl -X PUT -H "Content-Type:application/json" -d '{ "name":"lulz" }' http://localhost:3001/api/v1/bots
