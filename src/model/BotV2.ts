// import { SlackBot } from "slackbots"; TODO types
let SlackBot = require("slackbots");

export class BotV2 {
  private static globalId: number = 0;
  private _id: number;
  private slackUser: string;
  private currencyPair: string;
  private intervalle: string;
  /* TODO enum 1, 5, 15, 30, 60, 240, 1440, 10080, 21600 */

  // private bot: SlackBot;
  private static params = {
    icon_emoji: ":robot_face:"
  };

  constructor(slackUser: string, currencyPair: string, intervalle: string) {
    this._id = BotV2.globalId++;
    this.slackUser = slackUser;
    this.currencyPair = currencyPair;
    this.intervalle = intervalle;
    this.start();
  }

  get id(): number {
    return this._id;
  }

  public start() {
    console.log("bot started");
  }
}
