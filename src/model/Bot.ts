export class Bot {
  private static globalId: number = 0;

  id: number;
  name: string;

  constructor(name: string) {
    this.id = Bot.globalId++;
    this.name = name;
  }

  getName(): string {
    return this.name;
  }

  setName(newName: string) {
    this.name = newName;
  }

}
