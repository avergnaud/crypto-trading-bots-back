import { Router, Request, Response, NextFunction } from "express";
import { Bot } from "../model/Bot";

/*
https://stackoverflow.com/questions/37233735/typescript-interfaces-vs-types
https://stackoverflow.com/questions/42211175/typescript-hashmap-dictionary-interface
https://stackoverflow.com/questions/40976536/how-to-define-typescript-map-of-key-value-pair-where-key-is-a-number-and-value
http://2ality.com/2015/01/es6-maps-sets.html
*/
export class BotsRouter {

  router: Router;
  bots: Map<number, Bot>;

  constructor() {
    this.router = Router();
    this.router.put("/", this.putOne);
    this.router.get('/', this.getAll);
    this.router.post("/", this.postOne);
    this.router.get('/:id', this.getOne);
    this.router.delete('/:id', this.deleteOne);
    this.bots = new Map<number, Bot>();
    console.log(this.bots);
  }

  /* TODO http://blog.xebia.fr/2014/03/17/post-vs-put-la-confusion/ */
  public putOne = (req: Request, res: Response, next: NextFunction) => {
    console.log(req.body);

    let id = parseInt(req.body.id);
    console.log("id " + id);
    console.log("this.bots.has(id) " + this.bots.has(id));
    if(this.bots.has(id)) {
      // update
      let bot = this.bots.get(id);
      let newName = <string>req.body.name;
      bot.setName(newName);
    } else {
      // create
      let jsonObj = req.body;
      let newOne = new Bot(jsonObj['name']);
      this.bots.set(newOne.id, newOne);
    }
    res.send(req.body);
    
  }

  public postOne = (req: Request, res: Response, next: NextFunction) => {
    console.log(req.body);

    // create
    let jsonObj = req.body;
    let newOne = new Bot(jsonObj['name']);
    this.bots.set(newOne.id, newOne);
    res.send(JSON.stringify(newOne));
    
  }

  public getOne = (req: Request, res: Response, next: NextFunction) => {
    let id = parseInt(req.params.id);
    if(this.bots.has(id)) {
      res.send(this.bots.get(id));
    }
  }

  public getAll = (req: Request, res: Response, next: NextFunction) => {
    console.log("hello");
    console.log(this.bots);
    let values = Array.from( this.bots.values() );
    res.send(this.mapToJson(values));
}

public deleteOne = (req: Request, res: Response, next: NextFunction) => {
  let id = parseInt(req.params.id);
  if(this.bots.has(id)) {
    this.bots.delete(id);
  }
  // res.sendStatus(200);
  res.send("{}");
}

  public mapToJson(map): string {
    if(typeof map == 'undefined') {
        return '???'
    }
    return JSON.stringify([...map]);
  }
}

const botsRoutes = new BotsRouter();

export default botsRoutes.router;
